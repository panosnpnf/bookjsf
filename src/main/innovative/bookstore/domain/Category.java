package innovative.bookstore.domain;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "CATEGORY_ID_GENERATOR")
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROOT_ID")
    private Category root;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "root")
    private List<Category> subCategories;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
    private List<Book> books;

}
