package innovative.bookstore.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Order")
@Table(name = "ORDERS")
@TableGenerator(name = "ORDER_ID_GENERATOR",
        table = "SEQUENCE",
        pkColumnName = "SEQUENCE_NAME",
        valueColumnName = "SEQUENCE_VALUE",
        pkColumnValue = "ORDER_ID_SEQUENCE"
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ORDER_ID_GENERATOR")
    @Column(name = "ID")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CUSTOMER_ID", nullable = false, updatable = false)
    private Customer customer;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    private List<Item> items;

    //    /**
//     * Updates bi-directional relationships.
//     */
//    @PrePersist
//    public void prePersist() {
//        //Update Customer Orders
//        if (customer.getOrders() == null) {
//            customer.setOrders(new ArrayList<>());
//        }
//        if (!customer.getOrders().contains(this)) {
//            customer.getOrders().add(this);
//        }
//    }

//    /**
//     * Updates bi-directional relationships.
//     */
//    @PreRemove
//    public void preRemove() {
//        //Update Customer Order
//        if (customer.getOrders() != null) {
//            customer.getOrders().remove(this);
//        }
//    }
}

