package innovative.bookstore.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "User")
@Table(name = "USER")
@TableGenerator(name = "USER_ID_GENERATOR",
        table = "SEQUENCE",
        pkColumnName = "SEQUENCE_NAME",
        valueColumnName = "SEQUENCE_VALUE",
        pkColumnValue = "USER_ID_SEQUENCE"
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "USER_ID_GENERATOR")
    @Column(name = "ID")
    private Long id;

    @Column(name = "USERNAME", unique = true, nullable = false, updatable = false, length = 25)
    private String username;

    @Column(name = "PASSWORD", nullable = false, length = 512)
    private String password;

//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
//    private List<UserSecurityRole> userSecurityRoles;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "user")
    private Customer customer;

}

