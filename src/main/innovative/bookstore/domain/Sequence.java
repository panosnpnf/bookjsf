package innovative.bookstore.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity(name = "Sequence")
@Table(name = "SEQUENCE")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Sequence implements Serializable {

    @Id
    @Column(name = "SEQUENCE_NAME", length = 50)
    private String name;

    @Column(name = "SEQUENCE_VALUE")
    private Long value;

}

