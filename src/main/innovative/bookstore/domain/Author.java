package innovative.bookstore.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name = "Author")
@Table(name = "AUTHOR")
@TableGenerator(name = "AUTHOR_ID_GENERATOR",
        table = "SEQUENCE",
        pkColumnName = "SEQUENCE_NAME",
        valueColumnName = "SEQUENCE_VALUE",
        pkColumnValue = "AUTHOR_ID_SEQUENCE"
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Author implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "AUTHOR_ID_GENERATOR")
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @Column(name = "SURNAME", nullable = false, length = 100)
    private String surname;

    @Column(name = "AGE", nullable = false)
    private Integer age;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "author")
    private List<Book> books;


}

