package innovative.bookstore.domain;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name = "Customer")
@Table(name = "CUSTOMER")
@TableGenerator(name = "CUSTOMER_ID_GENERATOR",
        table = "SEQUENCE",
        pkColumnName = "SEQUENCE_NAME",
        valueColumnName = "SEQUENCE_VALUE",
        pkColumnValue = "CUSTOMER_ID_SEQUENCE"
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "CUSTOMER_ID_GENERATOR")
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @Column(name = "SURNAME", nullable = false, length = 100)
    private String surname;

    @Column(name = "AGE", nullable = false)
    private Integer age;

    @Column(name = "ADDRESS", nullable = false)
    private String address;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "USER_ID", unique = true, updatable = false)
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
    private List<Order> orders;

}
