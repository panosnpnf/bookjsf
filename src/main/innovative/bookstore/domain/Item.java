package innovative.bookstore.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "Item")
@Table(name = "ITEM")
@TableGenerator(name = "ITEM_ID_GENERATOR",
        table = "SEQUENCE",
        pkColumnName = "SEQUENCE_NAME",
        valueColumnName = "SEQUENCE_VALUE",
        pkColumnValue = "ITEM_ID_SEQUENCE"
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ITEM_ID_GENERATOR")
    @Column(name = "ID")
    private Long id;

    @Column(name = "QUANTITY", nullable = false)
    private Long quantity;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "BOOK_ISBN", nullable = false, updatable = false)
    private Book book;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ORDER_ID", nullable = false, updatable = false)
    private Order order;

    /**
     * Updates bi-directional relationships.
     */
//    @PrePersist
//    public void prePersist() {
//        //Update Book Items
//        if (book.getItems() == null) {
//            book.setItems(new ArrayList<>());
//        }
//        if (!book.getItems().contains(this)) {
//            book.getItems().add(this);
//        }
//
//        //Update Order Items
//        if (order.getItems() == null) {
//            order.setItems(new ArrayList<>());
//        }
//        if (!order.getItems().contains(this)) {
//            order.getItems().add(this);
//        }
//    }

//    /**
//     * Updates bi-directional relationships.
//     */
//    @PreRemove
//    public void preRemove() {
//        //Update Book Items
//        if (book.getItems() != null) {
//            book.getItems().remove(this);
//        }
//
//        //Update Order Items
//        if (order.getItems() != null) {
//            order.getItems().remove(this);
//        }
//    }
}

