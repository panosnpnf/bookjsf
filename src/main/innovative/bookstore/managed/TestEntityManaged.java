package innovative.bookstore.managed;

import innovative.bookstore.domain.TestEntity;
import innovative.bookstore.service.TestEntityService;
import innovative.bookstore.service.dto.TestEntityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.annotation.ManagedBean;
//import javax.faces.bean.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.annotation.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

@Component
@ManagedBean
public class TestEntityManaged implements Serializable {

    @Autowired
    private TestEntityService testEntityService;

    public String testString() {
        return this.testEntityService.getTestString();
    }

    private String message;

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public String getMe() {
        return "hello";
    }
}
