package innovative.bookstore.service;


import innovative.bookstore.service.dto.AuthorDTO;

import java.util.List;

public interface AuthorService {

    AuthorDTO save(AuthorDTO authorDTO);

    AuthorDTO update(AuthorDTO authorDTO);

    AuthorDTO findById(Long id);

    void delete(Long id);

    List<AuthorDTO> findAll();

    void deleteAll();
}
