package innovative.bookstore.service.dto;

import java.io.Serializable;
import java.util.List;

public class AuthorDTO implements Serializable {

    private Long id;

    private String name;

    private String surname;

    private Integer age;

    private List<String> books;
}
