package innovative.bookstore.service.impl;

import innovative.bookstore.repository.AuthorRepository;
import innovative.bookstore.service.AuthorService;
import innovative.bookstore.service.dto.AuthorDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorService {

    private final ModelMapper modelMapper = new ModelMapper();

////    @Autowired
//    private final AuthorRepository authorRepository;
//
//    @Autowired
//    public AuthorServiceImpl(AuthorRepository authorRepository) {
//        this.authorRepository = authorRepository;
//    }

    @Override
    public AuthorDTO save(AuthorDTO authorDTO) {
        return null;
    }

    @Override
    public AuthorDTO update(AuthorDTO authorDTO) {
        return null;
    }

    @Override
    public AuthorDTO findById(Long id) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<AuthorDTO> findAll() {
        return null;
    }

    @Override
    public void deleteAll() {

    }
}
