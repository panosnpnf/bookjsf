package innovative.bookstore.service.impl;

import innovative.bookstore.domain.TestEntity;
import innovative.bookstore.repository.TestEntityRepository;
import innovative.bookstore.service.TestEntityService;
import innovative.bookstore.service.dto.TestEntityDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class TestEntityServiceImpl implements TestEntityService {

    private final ModelMapper modelMapper = new ModelMapper();

//    private final TestEntityRepository testEntityRepository;
//
//    public TestEntityServiceImpl(TestEntityRepository testEntityRepository) {
//        this.testEntityRepository = testEntityRepository;
//    }
//
//    public TestEntityDTO save(TestEntityDTO testEntityDTO) {
//        TestEntity testEntity = modelMapper.map(testEntityDTO, TestEntity.class);
//        testEntity = testEntityRepository.save(testEntity);
//        return modelMapper.map(testEntity, TestEntityDTO.class);
//    }
//
//    public TestEntityDTO getOne(Long id) {
//        TestEntity testEntity = testEntityRepository.getOne(id);
//        return modelMapper.map(testEntity, TestEntityDTO.class);
//    }
//
//    public TestEntityDTO getTest() {
//        TestEntityDTO testEntityDTO = new TestEntityDTO();
//        Long something = new Long(10);
//        testEntityDTO.setId(something);
//        testEntityDTO.setFirstName("Kostas");
//        testEntityDTO.setLastName("Dimitriou");
//        return testEntityDTO;
//    }

    public TestEntityServiceImpl() {
    }

    @Override
    public String getTestString(){
        return "hooray!";
    }
}
